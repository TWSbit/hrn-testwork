module.exports = () => {
    $('.showpopup').on('click', (event) => $($(event.currentTarget).data('target')).addClass('active'));

    $('.close.button').on('click', (event) => {
        $('.videoplayer:visible').each((index, videoPlayer) => {
            videoPlayer.contentWindow.postMessage('{ "event": "command", "func": "stopVideo", "args": "" }', '*');
        });

        $(event.currentTarget).closest('.popup').removeClass('active');
    });
};

