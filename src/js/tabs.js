module.exports = () => {
    $('.tabs').each((tabsIndex, tabs) => {
        const $tabLinks = $(tabs).find('a');
        const $tabContents = $(tabs).nextAll('.tabcontent');
        const activeClass = 'active';
        let paused = false;
        let activeItem = -1;

        setInterval(() => {
            if (!paused) {
                let nextTabIndex = ++activeItem % $tabLinks.length;
                $tabLinks.removeClass(activeClass).eq(nextTabIndex).addClass(activeClass);
            }
        }, 2000);

        $tabLinks.each((tabIndex, tabLink) => {
            $(tabLink).on('click', (event) => {
                $tabLinks.removeClass(activeClass);
                $(event.currentTarget).addClass(activeClass);
                $tabContents.removeClass(activeClass).eq(tabIndex).addClass(activeClass);

                return false;
            });

            $(tabLink).hover(
                (event) => {
                    paused = true;
                    $tabLinks.removeClass(activeClass);
                    $(event.currentTarget).addClass(activeClass);
                },
                (event) => {
                    paused = false;
                    $(event.currentTarget).removeClass(activeClass);
                }
            );
        });
    });
};
