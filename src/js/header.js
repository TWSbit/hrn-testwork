module.exports = () => {
    $('.navbar').each(() => {
        const $menuLinks = $('#main-menu a');
        const delay = 100;
        const menuHideDelay = delay * $menuLinks.length;

        $menuLinks.each((index, menuItem) => {
            $(menuItem).css('transition-delay', index * delay + 'ms, 0s, 0s');
        });

        $('.burger-menu').on('click', () => {
            const $body = $('body');
            const activeClass = 'menu-active';
            const isMenuActive = $body.hasClass(activeClass);

            if (isMenuActive) {
                $('#main-menu').css('transition-delay', menuHideDelay + 'ms');
                $body.removeClass(activeClass);
            } else {
                $('#main-menu').css('transition-delay', '0ms');
                $body.addClass(activeClass);
            }
        });
    });
};
