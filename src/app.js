// Compile styles
require('./scss/style.scss');

import jQuery   from "jquery";
window.$ = window.jQuery = jQuery;

import Header   from "./js/header";
import Tabs     from "./js/tabs";
import Popup    from "./js/popup";

$(document).ready(() => {
    Header();
    Tabs();
    Popup();
});
